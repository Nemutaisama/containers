FROM php:8.0-fpm
# $(nproc) - Count CpuX in host
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN apt-get update && apt-get install -y --no-install-recommends \
        optipng \
        procps \
        libzip-dev \
        curl \
        rsync \
        nano \
        git \
        jpegoptim \
        libfreetype6-dev \
        libicu-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libmemcached-dev \
        libonig-dev \
        libpng-dev \
        libpq-dev \
        libxml2-dev \
#        libxpm-dev \
#        libvpx-dev \
        apt-transport-https \
        msmtp \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug \
    && echo "sendmail_path = /usr/bin/msmtp -t" >> /usr/local/etc/php/conf.d/msmtp.ini \
    && pecl install redis \
    && docker-php-ext-enable redis \
    && pecl install mcrypt \
    && docker-php-ext-enable mcrypt \
    && pecl install mongodb \
    && echo "extension=mongodb.so" > /usr/local/etc/php/conf.d/mongodb.ini \
    && docker-php-ext-install -j"$(nproc)" bcmath  \
    && docker-php-ext-install -j"$(nproc)" pdo_mysql \
    && docker-php-ext-install -j"$(nproc)" mysqli  \
    && docker-php-ext-install -j$(nproc) intl \
    && docker-php-ext-install -j$(nproc) zip \
    && docker-php-ext-install -j$(nproc) pgsql \
    && docker-php-ext-install -j$(nproc) pdo_pgsql \
    && docker-php-ext-install -j$(nproc) soap \
    && docker-php-ext-install -j$(nproc) mbstring \
    && docker-php-ext-install -j$(nproc) exif \
    && docker-php-ext-install -j$(nproc) pcntl \
    && docker-php-ext-install -j$(nproc) opcache \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini" \
    && cp "$PHP_INI_DIR/php.ini" "$PHP_INI_DIR/php-cli.ini" \
    && apt-get clean autoclean \
    && apt-get autoremove --yes \
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-source delete
